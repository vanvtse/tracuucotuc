﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TraCuuCoTuc.Data;
using TraCuuCoTuc.Helper;

namespace TraCuuCoTuc
{
    public partial class Form1
    {
        private void Update_Bank(object sender, EventArgs e)
        {
            //this.textBox_Bank_CMND.Text
            var query1 = dataContext.bank_account.Where(c => c.cmnd_number.Equals(textBox_Bank_CMND.Text));
            long bank_id;
            if (query1.Count() == 0)
            {
                // add new

                var bank = new bank_account
                {
                    bank = this.textBox_Bank_Name.Text,
                    beneficiaries = this.textBox_Bank_Benifier.Text,
                    cmnd_number = textBox_Bank_CMND.Text,
                    created = DateTime.Now,
                    number_account = this.textBox_Bank_Number.Text
                };
                dataContext.bank_account.Add(bank);
                dataContext.SaveChanges();
                bank_id = bank.id;
            }
            else
            {
                // update;
                var dataUpdate = query1.FirstOrDefault();
                bank_id = dataUpdate.id;
                dataUpdate.bank = this.textBox_Bank_Name.Text;
                dataUpdate.beneficiaries = this.textBox_Bank_Benifier.Text;
                dataUpdate.cmnd_number = textBox_Bank_CMND.Text;
                dataUpdate.created = DateTime.Now;
                dataUpdate.number_account = this.textBox_Bank_Number.Text;
            }
            var query2 = dataContext.transactions.Where(c => c.cnmd.Equals(textBox_Bank_CMND.Text)).Where(c => c.is_paid == false);
            foreach (var item in query2)
            {
                item.bank_account_id = bank_id;
            }
            dataContext.SaveChanges();
            RefreshUI();
        }


        private void Import_Bank(object sender, EventArgs e)
        {
            openFileDialog1 = new OpenFileDialog()
            {
                Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                Title = "Open excel file"
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var list = ExcelComunication.DataTableToBankAccount(ExcelComunication.ExcelToDataTable(openFileDialog1.FileName));
                    list = list.GroupBy(c => c.cmnd_number).Select(c => c.FirstOrDefault()).ToList();
                    dataContext.bank_account.AddRange(list);
                    //dataContext.Entry(list).State = EntityState.Added;
                    dataContext.SaveChanges();
                    //dataContext.modify_banks_transaction();
                    var listDeleteId = dataContext.bank_account.GroupBy(v => v.cmnd_number).Select(c => new {
                        count = c.Count(),
                        bank = c.FirstOrDefault()
                    }).Where(c => c.count > 1).Select(c => c.bank);
                    var listDelete = dataContext.bank_account.RemoveRange(listDeleteId);
                    dataContext.SaveChanges();
                    dataContext.init_transaction();
                    RefreshUI();
                    MessageBox.Show("Import thành công", "Thành công",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (System.IO.IOException ex)
                {
                    MessageBox.Show("Đóng file trước khi import", "File is opened",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        MessageBox.Show(ex.InnerException.Message, "Error",
                                      MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(ex.Message, "Error",
                                      MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }

            }
        }
    }
}
