﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraCuuCoTuc.Model
{
    public class DividendPersonVM
    {
        public int? Year { get; set; }
        public int? Batch { get; set; }
       
        public Nullable<decimal> number_stock_not_deposite { get; set; }
        public Nullable<decimal> number_stock_deposite { get; set; }

        public Nullable<decimal> dividend_not_deposite { get; set; }
        public Nullable<decimal> dividend_deposite { get; set; }

        public Nullable<decimal> tax_not_deposite { get; set; }
        public Nullable<decimal> tax_deposite { get; set; }
       
    }
}
