﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TraCuuCoTuc.Const;
using TraCuuCoTuc.Data;
using TraCuuCoTuc.Model;

namespace TraCuuCoTuc
{
    public partial class Form1
    {
        private void BindingdataGridView_bankAcounts()
        {
            var rs = this.dataContext.bank_account.OrderByDescending(e => e.id).Select(e => new
            {
                e.cmnd_number,
                e.beneficiaries,
                e.number_account,
                e.bank,
                e.created
            });
            this.dataGridView_bankAcounts.DataSource = rs.ToList();
            this.dataGridView_bankAcounts.Columns[0].HeaderText = "SỐ CMND";
            this.dataGridView_bankAcounts.Columns[1].HeaderText = "NGƯỜI THỤ HƯỞNG";
            this.dataGridView_bankAcounts.Columns[2].HeaderText = "SỐ TÀI KHOẢN";
            this.dataGridView_bankAcounts.Columns[3].HeaderText = "NGÂN HÀNG";
            this.dataGridView_bankAcounts.Columns[4].HeaderText = "NGÀY TẠO";
        }
        private void BindingDatalist_origin(IQueryable<original_listing> data)
        {
            var dataBind = data.Select(e => new
            {
                e.id,
                e.full_name,
                e.cmnd_number,
                e.cmnd_issue_day,
                e.address,
                e.country,
                e.number_stock_not_deposite,
                e.number_stock_deposited,
            
                e.number_stock_total,
                e.dividend_not_deposite,
                e.dividend_deposited,
               
                e.dividend_total,
                e.tax_not_deposite,
                e.tax_deposited,
                
                e.tax_total,
                e.dividend_received_not_deposite,
                e.dividend_received_deposited,
               
                e.dividend_received_total,
                e.type,
                e.assignment_organization,
                e.txnum,
                e.email,
                e.phone,
                e.note
            }).ToList();
            this.dataGridView_list_origin.DataSource = dataBind;
            this.dataGridView_list_origin.Columns[0].HeaderText = "STT";
            this.dataGridView_list_origin.Columns[1].HeaderText = "HỌ TÊN(0)";
            this.dataGridView_list_origin.Columns[2].HeaderText = "SỐ ĐKSH(1)";
            this.dataGridView_list_origin.Columns[3].HeaderText = "NGÀY CẤP(2)";
            this.dataGridView_list_origin.Columns[4].HeaderText = "ĐỊA CHỈ LIÊN HỆ(3) ";
            this.dataGridView_list_origin.Columns[5].HeaderText = "QUỐC TỊCH(6)";
            this.dataGridView_list_origin.Columns[6].HeaderText = "CHƯA LK(7)";
            this.dataGridView_list_origin.Columns[7].HeaderText = "ĐÃ LK(8)";
            this.dataGridView_list_origin.Columns[8].HeaderText = "CỘNG(9)";
            this.dataGridView_list_origin.Columns[9].HeaderText = "CHƯA LK(10)";
            this.dataGridView_list_origin.Columns[10].HeaderText = "ĐÃ LK(11)";
            this.dataGridView_list_origin.Columns[11].HeaderText = "CỘNG (12)";
            this.dataGridView_list_origin.Columns[12].HeaderText = "CHƯA LK(13)";
            this.dataGridView_list_origin.Columns[13].HeaderText = "ĐÃ LK(14)";
            this.dataGridView_list_origin.Columns[14].HeaderText = "CỘNG (15)";
            this.dataGridView_list_origin.Columns[15].HeaderText = "CHƯA LK(16)";
            this.dataGridView_list_origin.Columns[16].HeaderText = "ĐÃ LK(17)";
            this.dataGridView_list_origin.Columns[17].HeaderText = "CỘNG (18)";
            this.dataGridView_list_origin.Columns[18].HeaderText = "TYPE";
            this.dataGridView_list_origin.Columns[19].HeaderText = "CNTC";
            this.dataGridView_list_origin.Columns[20].HeaderText = "TXNUM";
            this.dataGridView_list_origin.Columns[21].HeaderText = "EMAIL(4)";
            this.dataGridView_list_origin.Columns[22].HeaderText = "ĐIỆN THOẠI(5)";
            this.dataGridView_list_origin.Columns[23].HeaderText = "GHI CHÚ(19)";
            this.dataGridView_list_origin.ColumnHeadersHeight = 60;
            this.dataGridView_list_origin.Columns[6].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[7].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[8].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[9].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[10].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[11].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[12].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[13].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[14].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[15].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[16].DefaultCellStyle.Format = "N0";
            this.dataGridView_list_origin.Columns[17].DefaultCellStyle.Format = "N0";

            if (PageIndex == 1)
            {
                this.linkLabel1.Enabled = false;
                this.linkLabel2.Enabled = false;
            }
            else
            {
                this.linkLabel1.Enabled = true;
                this.linkLabel2.Enabled = true;
            }
            if (PageIndex == MaxIndex)
            {
                this.linkLabel3.Enabled = false;
                this.linkLabel4.Enabled = false;
            }
            else
            {
                this.linkLabel3.Enabled = true;
                this.linkLabel4.Enabled = true;
            }
        }
        private void BindingDataTransactionBank()
        {
            var rs = this.dataContext.transactions.Where(e => e.bank_account != null).Where(e => e.is_paid == false).Select(e => new
            {
                e.cnmd,
                e.full_name,
                e.bank_account.beneficiaries,
                e.bank_account.number_account,
                e.bank_account.bank,
                e.receive_money,
                e.tax,
                e.real_recevie_money
            });
            this.dataGridView_cknh.DataSource = rs.ToList();
            this.dataGridView_cknh.Columns[0].HeaderText = "SỐ CMND";
            this.dataGridView_cknh.Columns[1].HeaderText = "HỌ VÀ TÊN";
            this.dataGridView_cknh.Columns[2].HeaderText = "NGƯỜI THỤ HƯỞNG";
            this.dataGridView_cknh.Columns[3].HeaderText = "SỐ TÀI KHOẢN";
            this.dataGridView_cknh.Columns[4].HeaderText = "NGÂN HÀNG";
            this.dataGridView_cknh.Columns[5].HeaderText = "CỔ TỨC";
            this.dataGridView_cknh.Columns[6].HeaderText = "THUẾ TNCN 5%";
            this.dataGridView_cknh.Columns[7].HeaderText = "CÒN ĐƯỢC NHẬN";
        }
        private void BindingDataTransactionBankHistory()
        {
            var rs = dataContext.transactions.Where(e => e.bank_account != null).Where(e => e.is_paid == true).Select(e => new
            {
                e.id,
                e.cnmd,
                type = MessageConst.BANK,
                e.full_name,
                e.receive_money,
                e.tax,
                e.real_recevie_money
            });
            this.dataGridView_tran_bank.DataSource = rs.ToList();
            this.dataGridView_tran_bank.Columns[0].HeaderText = "Mã giao dịch";
            this.dataGridView_tran_bank.Columns[1].HeaderText = "Số CMND";
            this.dataGridView_tran_bank.Columns[2].HeaderText = "Loại giao dịch";
            this.dataGridView_tran_bank.Columns[3].HeaderText = "Họ và tên";

            this.dataGridView_tran_bank.Columns[4].HeaderText = "Số tiền được nhận";
            this.dataGridView_tran_bank.Columns[5].HeaderText = "Thuế TNCN 5%";
            this.dataGridView_tran_bank.Columns[6].HeaderText = "Thực nhận";

            this.dataGridView_tran_bank.Columns[4].DefaultCellStyle.Format = "N0";
            this.dataGridView_tran_bank.Columns[5].DefaultCellStyle.Format = "N0";
            this.dataGridView_tran_bank.Columns[6].DefaultCellStyle.Format = "N0";

        }
        private void BindingdataGridView_changeHistory(IQueryable<change_history> data)
        {
            this.dataGridView_changeHistory.DataSource = data.ToList();
            this.dataGridView_changeHistory.Columns[0].HeaderText = "Id";
            this.dataGridView_changeHistory.Columns[1].HeaderText = "Nội dung";
            this.dataGridView_changeHistory.Columns[2].HeaderText = "Số CMND";
            this.dataGridView_changeHistory.Columns[3].HeaderText = "Ngày thay đổi";

        }
        private void BingdingDataNotPayYet()
        {
            var rs = dataContext.transactions.Where(e => !e.is_paid).Select(e => new
            {
                e.cnmd,
                e.full_name,
                e.receive_money,
                e.tax,
                e.real_recevie_money
            });
            this.dataGridView_not_pay_yet.DataSource = rs.ToList();
            this.dataGridView_not_pay_yet.Columns[0].HeaderText = "Số CMND";
            this.dataGridView_not_pay_yet.Columns[1].HeaderText = "Họ và tên";
            this.dataGridView_not_pay_yet.Columns[2].HeaderText = "Số tiền được nhận";
            this.dataGridView_not_pay_yet.Columns[3].HeaderText = "Thuế TNCN 5%";
            this.dataGridView_not_pay_yet.Columns[4].HeaderText = "Thực nhận";

            this.dataGridView_not_pay_yet.Columns[2].DefaultCellStyle.Format = "N0";
            this.dataGridView_not_pay_yet.Columns[3].DefaultCellStyle.Format = "N0";
            this.dataGridView_not_pay_yet.Columns[4].DefaultCellStyle.Format = "N0";
        }
        private void BindingDataTransactionCashHistory()
        {
            var rs = dataContext.transactions.Where(e => e.bank_account == null).Where(e => e.is_paid == true).Select(e => new
            {
                e.id,
                e.cnmd,
                type = MessageConst.CASH,
                e.full_name,
                e.receive_money,
                e.tax,
                e.real_recevie_money
            });
            this.dataGridView_transaction_cash.DataSource = rs.ToList();
            this.dataGridView_transaction_cash.Columns[0].HeaderText = "Mã giao dịch";
            this.dataGridView_transaction_cash.Columns[1].HeaderText = "Số CMND";
            this.dataGridView_transaction_cash.Columns[2].HeaderText = "Loại giao dịch";
            this.dataGridView_transaction_cash.Columns[3].HeaderText = "Họ và tên";
            this.dataGridView_transaction_cash.Columns[4].HeaderText = "Số tiền được nhận";
            this.dataGridView_transaction_cash.Columns[5].HeaderText = "Thuế TNCN 5%";
            this.dataGridView_transaction_cash.Columns[6].HeaderText = "Thực nhận";

            this.dataGridView_transaction_cash.Columns[4].DefaultCellStyle.Format = "N0";
            this.dataGridView_transaction_cash.Columns[5].DefaultCellStyle.Format = "N0";
            this.dataGridView_transaction_cash.Columns[6].DefaultCellStyle.Format = "N0";

        }
        private void BindingDividendPerson()
        {
            var dataDividendPerson = dataContext.original_listing.Where(x => x.cmnd_number.Contains(this.label_cmnd.Text.Trim()))
     .Select(v => new
     {
         Year = v.year,
         Batch = v.batch,
         number_stock_not_deposite = v.number_stock_not_deposite,
         dividend_not_deposite = v.dividend_not_deposite,
         tax_not_deposite = v.tax_not_deposite,

         number_stock_deposite = v.number_stock_deposited,
         dividend_deposite = v.dividend_deposited,
         tax_deposite = v.tax_deposited,

     });
            this.dataGridView_DividendPersonVM.DataSource = dataDividendPerson.ToList();
            this.dataGridView_DividendPersonVM.Columns[0].HeaderText = "Năm";
            this.dataGridView_DividendPersonVM.Columns[1].HeaderText = "Đợt";

            this.dataGridView_DividendPersonVM.Columns[2].HeaderText = "CP chưa LK";
            this.dataGridView_DividendPersonVM.Columns[3].HeaderText = "Tiền chưa LK";
            this.dataGridView_DividendPersonVM.Columns[4].HeaderText = "Thuế 5% chưa LK";

            this.dataGridView_DividendPersonVM.Columns[5].HeaderText = "CP đã LK";
            this.dataGridView_DividendPersonVM.Columns[6].HeaderText = "Tiền đã LK";
            this.dataGridView_DividendPersonVM.Columns[7].HeaderText = "Thuế 5% đã LK";

            this.dataGridView_DividendPersonVM.Columns[2].DefaultCellStyle.Format = "N0";
            this.dataGridView_DividendPersonVM.Columns[3].DefaultCellStyle.Format = "N0";
            this.dataGridView_DividendPersonVM.Columns[4].DefaultCellStyle.Format = "N0";
            this.dataGridView_DividendPersonVM.Columns[5].DefaultCellStyle.Format = "N0";
            this.dataGridView_DividendPersonVM.Columns[6].DefaultCellStyle.Format = "N0";
            this.dataGridView_DividendPersonVM.Columns[7].DefaultCellStyle.Format = "N0";

        }
        private void BindingDataGridView_transaciton_history_person(string cmnd)
        {
            var rs = dataContext.transactions.Where(c => c.cnmd.Contains(cmnd)).Where(c => c.is_paid == true).Select(e => new
            {
                e.id,
                e.cnmd,
                type = e.bank_account == null ? MessageConst.CASH : MessageConst.BANK,
                e.full_name,
                e.receive_money,
                e.tax,
                e.real_recevie_money
            });
            this.dataGridView_transaciton_history_person.DataSource = rs.ToList();
            this.dataGridView_transaciton_history_person.Columns[0].HeaderText = "Mã giao dịch";
            this.dataGridView_transaciton_history_person.Columns[1].HeaderText = "Số CMND";
            this.dataGridView_transaciton_history_person.Columns[2].HeaderText = "Loại giao dịch";
            this.dataGridView_transaciton_history_person.Columns[3].HeaderText = "Họ và tên";

            this.dataGridView_transaciton_history_person.Columns[4].HeaderText = "Số tiền được nhận";
            this.dataGridView_transaciton_history_person.Columns[5].HeaderText = "Thuế TNCN 5%";
            this.dataGridView_transaciton_history_person.Columns[6].HeaderText = "Thực nhận";

            this.dataGridView_transaciton_history_person.Columns[4].DefaultCellStyle.Format = "N0";
            this.dataGridView_transaciton_history_person.Columns[5].DefaultCellStyle.Format = "N0";
            this.dataGridView_transaciton_history_person.Columns[6].DefaultCellStyle.Format = "N0";
        }

    }
}
