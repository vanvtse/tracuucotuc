﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraCuuCoTuc.Model
{
    public class BankTransactionVM
    {
        public long id { get; set; }
        public string cmnd_number { get; set; }
        public string full_name { get; set; }
        public string beneficiaries { get; set; }
        public string number_account { get; set; }
        public string bank { get; set; }
        public decimal? stock_number { get; set; }
        public decimal? dividend { get; set; }
        public decimal? tax { get; set; }
        public decimal? real_recieve { get; set; }
    }
}
