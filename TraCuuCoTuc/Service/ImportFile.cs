﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TraCuuCoTuc.Helper;

namespace TraCuuCoTuc
{
    public partial class Form1
    {
        private async Task ImportFileOringinListAsync()
        {
            string yearString = Prompt.ShowDialog("Nhập danh sách cho năm", "Nhập năm");
            int year = 0;
            int batch = 0;
            try
            {
                year = int.Parse(yearString);
                batch = dataContext.original_listing.OrderByDescending(v => v.batch).Where(c => c.year == year).FirstOrDefault()?.batch ?? 0;
                batch++;
                openFileDialog1 = new OpenFileDialog()
                {
                    Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                    Title = "Open excel file"
                };
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {

                        var list = ExcelComunication.DataTableTooriginal_listing(ExcelComunication.ExcelToDataTable(openFileDialog1.FileName), year, batch);
                        dataContext.original_listing.AddRange(list);

                        await dataContext.SaveChangesAsync();
                        dataContext.init_transaction();
                        RefreshUI();
                        MessageBox.Show("Import thành công", "Thành công",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (System.IO.IOException ex)
                    {
                        MessageBox.Show("Đóng file trước khi import", "File is opened",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null)
                        {
                            MessageBox.Show(ex.InnerException.Message, "Error",
                                          MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            MessageBox.Show(ex.Message, "Error",
                                          MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }

                }
            }
            catch (Exception ec)
            {


            }

        }
    }
}
