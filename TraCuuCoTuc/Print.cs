﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TraCuuCoTuc.Helper;

namespace TraCuuCoTuc
{
    public partial class Print : Form
    {

        public Print(string rp_cmnd, DateTime rp_create_date, long rp_Id_transaction, string rp_reason, decimal rp_tax,
            decimal rp_money_recv,decimal rp_money_recv_label, string rp_address,string  rp_fullname_recver)
        {
            InitializeComponent();
            CrystalReport1 cr = new CrystalReport1();
            cr.SetParameterValue("rp_cmnd", rp_cmnd);
            cr.SetParameterValue("rp_create_date", $"Ngày {rp_create_date.Day} Tháng {rp_create_date.Month} Năm {rp_create_date.Year}");
            cr.SetParameterValue("rp_Id_transaction", $"{rp_Id_transaction}/{rp_create_date.Year}");
            cr.SetParameterValue("rp_reason", rp_reason);
            cr.SetParameterValue("rp_tax", rp_tax.ToString("N0") +" đồng");
            cr.SetParameterValue("rp_money_recv", rp_money_recv.ToString("N0") + " đồng");
            cr.SetParameterValue("rp_money_recv_label", ConverterMoney.NumberToTextVN( rp_money_recv_label));
            cr.SetParameterValue("rp_address", rp_address);
            cr.SetParameterValue("rp_fullname_recver", rp_fullname_recver);
            crystalReportViewer1.ReportSource = cr;
            crystalReportViewer1.Refresh();
           

        }
    }
}
