﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraCuuCoTuc.Const
{
    public class MessageConst
    {
        public const string CASH = "Tiền mặt";
        public const string BANK = "Chuyển khoản";
        public const string NumberFormat = "{0:#,##0}";
    }
}
