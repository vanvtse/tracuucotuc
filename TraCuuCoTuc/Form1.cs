﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TraCuuCoTuc.Const;
using TraCuuCoTuc.Data;
using TraCuuCoTuc.Helper;
using TraCuuCoTuc.Model;
using EntityState = System.Data.Entity.EntityState;

namespace TraCuuCoTuc
{
    public partial class Form1 : Form
    {
        SecuritiStockEntities dataContext;
        private bool EditMode = true;
        private int PageIndex = 1;
        private int PageSize = 50;
        private int MaxIndex = 0;
        public Form1()
        {
            try
            {
                dataContext = new SecuritiStockEntities();
                MaxIndex = (int)Math.Ceiling((decimal)dataContext.original_listing.Count() / (decimal)PageSize);
                InitializeComponent();
                RefreshUI();
                ChangeInform();
                AlignPagingIcon();
            }
            catch (Exception e )
            {

                MessageBox.Show(e.Message);
            }


        }



        private void btn_ImportFile_Click(object sender, EventArgs e)
        {
            ImportFileOringinListAsync();

        }
        private void RefreshUI()
        {
            this.label_pageindex.Text = "" + PageIndex;
            BindingDatalist_origin(dataContext.original_listing.OrderBy(e => e.id).Skip((PageIndex - 1) * PageSize).Take(PageSize));
            BindingDataTransactionBank();
            BindingDataTransactionBankHistory();
            BindingDataTransactionCashHistory();
            BingdingDataNotPayYet();

            this.label_paid_MoneyTotal.Text = "" + string.Format("{0:#,##0}", dataContext.transactions.Where(e=>e.is_paid==true).Sum(e => e.receive_money));
            this.label_paid_StockTotal.Text = "" + string.Format("{0:#,##0}", dataContext.transactions.Where(e=>e.is_paid).Count());
            label_paid_Cash.Text = "" + string.Format("{0:#,##0}", dataContext.transactions.Where(e=>e.bank_account==null).Where(e=>e.is_paid==true).Sum(e => e.receive_money));
            label_paid_StockCash.Text = "" + string.Format("{0:#,##0}", dataContext.transactions.Where(e => e.bank_account == null).Where(e=>e.is_paid==true).Count());
            label_paid_Bank.Text = "" + string.Format("{0:#,##0}", dataContext.transactions.Where(e => e.bank_account != null).Where(e=>e.is_paid).Sum(e => e.receive_money));
            label_paid_StockBank.Text = "" + string.Format("{0:#,##0}", dataContext.transactions.Where(e => e.bank_account != null).Where(e => e.is_paid==true).Count());
            label_Paid_Money_Total.Text = "" + string.Format("{0:#,##0}", dataContext.transactions.Where(e=>e.is_paid==true).Sum(e => e.receive_money));
            label_NOT_Paid_Money_Total.Text = "" + string.Format("{0:#,##0}", dataContext.transactions.Where(e => e.is_paid == false).Sum(e => e.receive_money));
            label_Stock_Paid.Text = "" + string.Format("{0:#,##0}", dataContext.transactions.Where(e=>e.is_paid==true).Count());
            label_Stock_NOT_Paid.Text = "" + string.Format("{0:#,##0}", dataContext.transactions.Where(e => e.is_paid == false).Count());
            BindingdataGridView_bankAcounts();
        }
  
        private void textBox_cmnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SearchInforByCMND();
                e.Handled = true;
            }
        }

        private void button_pay_Click(object sender, EventArgs e)
        {
            var reason = textBox_reason.Text;
            var data = dataContext.transactions.Where(x => x.cnmd.Equals(this.label_cmnd.Text));
            foreach (var item in data)
            {
                item.is_paid = true;
                item.paid_date = DateTime.Now;
                item.type = "cash";
            }
        
            dataContext.SaveChanges();
            BindingDataGridView_transaciton_history_person(this.textBox_cmnd_search.Text);
            this.textBox_cmnd_search.Text = this.label_cmnd.Text;
            SearchInforByCMND();

            RefreshUI();
            //Print printfo = new Print(this.label_cmnd.Text, DateTime.Now, data.FirstOrDefault().id, reason, data.Sum(c=>c.tax).Value, 
            //    data.Sum(c=>c.real_recevie_money).Value, data.Sum(c => c.real_recevie_money).Value, this.label_address.Text, data.FirstOrDefault().full_name);
            //printfo.Show();
        }

        private void ChangeInform()
        {
            this.EditMode = !this.EditMode;
            this.textBox_cmnd.Visible = EditMode;
            this.textBox_issue.Visible = EditMode;
            this.textBox_fullname.Visible = EditMode;
            this.textBox_fullname_rever.Visible = !EditMode;
            this.textBox_address.Visible = EditMode;
            this.button_confirm.Visible = EditMode;
            this.button_cancel.Visible = EditMode;
            this.button_changeInfor.Visible = !EditMode;

            this.textBox_cmnd.Text = this.label_cmnd.Text;
            this.textBox_address.Text = this.label_address.Text;
            this.textBox_fullname.Text = this.textBox_fullname_rever.Text;
            this.textBox_issue.Text = this.label_cmnd_issue.Text;
        }

        private void button_changeInfor_Click(object sender, EventArgs e)
        {
            ChangeInform();
        }

        private void button_confirm_Click(object sender, EventArgs e)
        {
            UpdateOrAddBank(sender, e);
        }
        private void button_cancel_Click(object sender, EventArgs e)
        {
            ChangeInform();
        }

        
        private void AlignPagingIcon()
        {
            this.panel2.Left = this.ClientSize.Width / 2 - 75;
        }
        private void Form1_Resize(object sender, EventArgs e)
        {
            AlignPagingIcon();
        }

        private void button_pay_cknh_Click(object sender, EventArgs e)
        {

            pay_cknh(sender, e);
        }

        private void button_Update_Bank_Click(object sender, EventArgs e)
        {
            Update_Bank(sender, e);
        }


        private void button_Import_Bank_Click_1(object sender, EventArgs e)
        {
            Import_Bank(sender, e);
        }
    }
}

