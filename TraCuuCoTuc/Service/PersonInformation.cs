﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TraCuuCoTuc.Const;
using TraCuuCoTuc.Data;

namespace TraCuuCoTuc
{
    public partial class Form1
    {
        private void UpdateOrAddBank(object sender, EventArgs e)
        {


            //update orin list
            var bankAcconts = dataContext.bank_account.Where(x => x.cmnd_number.Equals(label_cmnd.Text));
            var originList = dataContext.original_listing.Where(x => x.cmnd_number.Equals(label_cmnd.Text));
            var fullName_check = originList.FirstOrDefault()?.full_name;
            var transactions = dataContext.transactions.Where(x => x.cnmd.Equals(label_cmnd.Text));
            var historyList = dataContext.change_history.Where(x => x.cmnd_number.Equals(label_cmnd.Text));
            foreach (var item in originList)
            {
                item.cmnd_number = textBox_cmnd.Text;
                item.address = textBox_address.Text;
                item.full_name = textBox_fullname.Text;
                item.cmnd_issue_day = textBox_issue.Text;
            }
            foreach (var item in transactions)
            {
                item.cnmd = textBox_cmnd.Text;
                item.full_name = textBox_fullname.Text;
            }
            foreach (var item in bankAcconts)
            {
                item.cmnd_number = textBox_cmnd.Text;
                item.beneficiaries = textBox_fullname.Text;
            }
            foreach (var item in historyList)
            {
                item.cmnd_number = textBox_cmnd.Text;
            }
            string content = "Đổi giá trị";
            if (!textBox_cmnd.Text.Equals(this.label_cmnd.Text))
            {
                content += $" số CMND từ {label_cmnd.Text} sang {textBox_cmnd.Text},";
            }
            if (!textBox_address.Text.Equals(this.label_address.Text))
            {
                content += $" địa chỉ từ {label_address.Text} sang {textBox_address.Text},";
            }
            if (!textBox_fullname.Text.Equals(fullName_check))
            {
                content += $" họ và tên từ {fullName_check} sang {textBox_fullname.Text},";
            }
            if (!textBox_issue.Text.Equals(this.label_cmnd_issue.Text))
            {
                content += $" ngày cấp từ {label_cmnd_issue.Text} sang {textBox_issue.Text}";
            }
            var history = new change_history
            {
                cmnd_number = !textBox_cmnd.Text.Equals(this.label_cmnd.Text) ? textBox_cmnd.Text : label_cmnd.Text,
                Content = content,
                created = DateTime.Now
            };
            if (!content.Equals("Đổi giá trị"))
            {
                dataContext.change_history.Add(history);
                dataContext.SaveChanges();
            }
            SearchInforByCMND();
            ChangeInform();
        }
        private void SearchInforByCMND()
        {
            var dataSeacrh = dataContext.original_listing.Where(x => x.cmnd_number.Contains(this.textBox_cmnd_search.Text));
            if (dataSeacrh.Count() == 0)
            {
                MessageBox.Show("Không tìm thấy: " + this.textBox_cmnd_search.Text);
            }
            else
            {
                var data = dataContext.transactions.Where(x => x.cnmd.Equals(dataSeacrh.FirstOrDefault().cmnd_number));
                var display = dataSeacrh.FirstOrDefault();
                data = data.Where(e => e.is_paid == false);
                this.label_cmnd.Text = display.cmnd_number.Trim();
                this.label_address.Text = display.address;
                this.textBox_fullname_rever.Text = display.full_name;
                this.label_cmnd_issue.Text = display.cmnd_issue_day;
                this.label_tax.Text = "" + string.Format(MessageConst.NumberFormat, data.Sum(c => c.tax));
                this.textBox_reason.Text = "";
                data.ToList().ForEach(x =>
                {

                    try
                    {
                        this.textBox_reason.Text += $"Cổ tức năm: {x.original_listing.year} đợt {x.original_listing.batch} {x.original_listing.number_stock_not_deposite}CP x {string.Format("{0:#,##0}", x.original_listing.dividend_not_deposite / x.original_listing.number_stock_not_deposite)}đ = {string.Format("{0:#,##0}", x.original_listing.dividend_not_deposite)}";
                        this.textBox_reason.AppendText(". ");
                    }
                    catch (System.DivideByZeroException)
                    {

                    }
                });
                this.label_money.Text = string.Format("{0:#,##0}", data.Sum(e => e.real_recevie_money));
                this.textBox_pay_money.Text = "" + data.Sum(e => e.real_recevie_money);
                //
                BindingDividendPerson();
                BindingDataGridView_transaciton_history_person(display.cmnd_number.Trim());
                BindingdataGridView_changeHistory(dataContext.change_history.Where(c => c.cmnd_number.Equals(display.cmnd_number.Trim())));
                this.button_pay.Enabled = data.Sum(ec => ec.real_recevie_money) > 0;
            }
        }
    }
}
