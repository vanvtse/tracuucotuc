﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TraCuuCoTuc.Data;

namespace TraCuuCoTuc.Helper
{
    class  ExcelComunication
    {
        public static DataTable ExcelToDataTable(string filename)
        {
            DataTable dt = new DataTable();
            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filename, false))
            {

                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                foreach (Cell cell in rows.ElementAt(0))
                {
                    dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                }
              
                foreach (Row row in rows) //this will also include your header row...
                {
                    DataRow tempRow = dt.NewRow();

                    for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                    {
                        try
                        {
                            tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                        }
                        catch (Exception)
                        {

                            tempRow[i] = "";
                        }
                        
                    }

                    dt.Rows.Add(tempRow);
                }
                dt.Rows.RemoveAt(0); //...s
            }
            return dt;
        }
        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
               
               
                    return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                
  
            }
            else
            {
                return value;
            }
        }
        public static List<original_listing> DataTableTooriginal_listing(DataTable data,int year,int batch)
        {
            List<original_listing> datas = new List<original_listing>();
            foreach (DataRow item in data.Rows)
            {
               //var dd =  item[0-1];
                try
                {
                    var tmp = new original_listing
                    {
                        full_name = item[1-1].ToString().Trim(),
                        cmnd_number = item[2-1].ToString().Trim(),
                        cmnd_issue_day = item[3-1].ToString().Trim(),
                        address = item[4-1].ToString().Trim(),
                        country = item[5-1].ToString().Trim(),
                        number_stock_not_deposite  = decimal.Parse(item[6-1].ToString().Trim()),
                        number_stock_deposited = decimal.Parse(item[7-1].ToString().Trim()),
                        number_stock_total = decimal.Parse(item[8-1].ToString().Trim()),
                        dividend_not_deposite = decimal.Parse(item[9-1].ToString().Trim()),
                        dividend_deposited  = decimal.Parse(item[10-1].ToString().Trim()),
                        dividend_total = decimal.Parse(item[11-1].ToString().Trim()),
                        tax_not_deposite  = decimal.Parse(item[12-1].ToString().Trim()),
                        tax_deposited = decimal.Parse(item[13-1].ToString().Trim()),
                        tax_total = decimal.Parse(item[14-1].ToString().Trim()),
                        dividend_received_not_deposite  = decimal.Parse(item[15-1].ToString().Trim()),
                        dividend_received_deposited = decimal.Parse(item[16-1].ToString().Trim()),
                        dividend_received_total = decimal.Parse(item[17-1].ToString().Trim()),
                        type = item[18-1].ToString().Trim(),
                        assignment_organization = item[19-1].ToString().Trim(),
                        txnum = item[20-1].ToString().Trim(),
                        email = item[21-1].ToString().Trim(),
                        phone = item[22-1].ToString().Trim(),
                        note = item[23-1].ToString().Trim(),
                        year = year,
                        batch = batch

                    };
                    datas.Add(tmp);
                }
                catch (System.FormatException)
                {

                }
               
            }
            return datas;
        }
        public static List<bank_account> DataTableToBankAccount(DataTable data)
        {
            List<bank_account> datas = new List<bank_account>();
            foreach (DataRow item in data.Rows)
            {
                //var dd = item[0-1];
                try
                {
                    var tmp = new bank_account
                    {
                        cmnd_number = item[0].ToString().Trim().Trim(),
                        beneficiaries = item[1].ToString().Trim(),
                        number_account = item[2].ToString().Trim(),
                        bank = item[3].ToString().Trim(),
                        created = DateTime.Now
                    };
                    datas.Add(tmp);
                }
                catch (System.FormatException)
                {

                }

            }
            return datas;
        }
    }

}
