﻿namespace TraCuuCoTuc
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridView_transaciton_history_person = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridView_DividendPersonVM = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView_changeHistory = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_fullname_rever = new System.Windows.Forms.TextBox();
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_confirm = new System.Windows.Forms.Button();
            this.textBox_issue = new System.Windows.Forms.TextBox();
            this.textBox_address = new System.Windows.Forms.TextBox();
            this.textBox_cmnd = new System.Windows.Forms.TextBox();
            this.textBox_fullname = new System.Windows.Forms.TextBox();
            this.textBox_reason = new System.Windows.Forms.TextBox();
            this.button_changeInfor = new System.Windows.Forms.Button();
            this.label_cmnd = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_pay_money = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button_pay = new System.Windows.Forms.Button();
            this.label_cmnd_issue = new System.Windows.Forms.Label();
            this.label_money = new System.Windows.Forms.Label();
            this.label_tax = new System.Windows.Forms.Label();
            this.label_address = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_cmnd_search = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.dataGridView_cknh = new System.Windows.Forms.DataGridView();
            this.button_pay_cknh = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.button_Import_Bank = new System.Windows.Forms.Button();
            this.dataGridView_bankAcounts = new System.Windows.Forms.DataGridView();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.button_Update_Bank = new System.Windows.Forms.Button();
            this.textBox_Bank_Name = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox_Bank_Number = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox_Bank_Benifier = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox_Bank_CMND = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label_pageindex = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.dataGridView_list_origin = new System.Windows.Forms.DataGridView();
            this.btn_ImportFile = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dataGridView_tran_bank = new System.Windows.Forms.DataGridView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dataGridView_transaction_cash = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label_paid_StockBank = new System.Windows.Forms.Label();
            this.label_paid_StockCash = new System.Windows.Forms.Label();
            this.label_paid_Bank = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label_paid_Cash = new System.Windows.Forms.Label();
            this.label_paid_StockTotal = new System.Windows.Forms.Label();
            this.label_paid_MoneyTotal = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label_Stock_NOT_Paid = new System.Windows.Forms.Label();
            this.label_Stock_Paid = new System.Windows.Forms.Label();
            this.label_NOT_Paid_Money_Total = new System.Windows.Forms.Label();
            this.label_Paid_Money_Total = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.dataGridView_not_pay_yet = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_transaciton_history_person)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_DividendPersonVM)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_changeHistory)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_cknh)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_bankAcounts)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_list_origin)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_tran_bank)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_transaction_cash)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_not_pay_yet)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1210, 943);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Controls.Add(this.textBox_cmnd_search);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1202, 910);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Thanh toán tiền mặt";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(24, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 20);
            this.label9.TabIndex = 12;
            this.label9.Text = "Tìm kiếm";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.dataGridView_transaciton_history_person);
            this.groupBox4.Location = new System.Drawing.Point(819, 531);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(380, 358);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Tra cứu lịch sử giao dịch";
            // 
            // dataGridView_transaciton_history_person
            // 
            this.dataGridView_transaciton_history_person.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_transaciton_history_person.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_transaciton_history_person.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_transaciton_history_person.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_transaciton_history_person.Location = new System.Drawing.Point(6, 25);
            this.dataGridView_transaciton_history_person.Name = "dataGridView_transaciton_history_person";
            this.dataGridView_transaciton_history_person.Size = new System.Drawing.Size(368, 327);
            this.dataGridView_transaciton_history_person.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.dataGridView_DividendPersonVM);
            this.groupBox3.Location = new System.Drawing.Point(819, 62);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(380, 463);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tra cứu cổ tức";
            // 
            // dataGridView_DividendPersonVM
            // 
            this.dataGridView_DividendPersonVM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_DividendPersonVM.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_DividendPersonVM.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_DividendPersonVM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_DividendPersonVM.Location = new System.Drawing.Point(6, 23);
            this.dataGridView_DividendPersonVM.Name = "dataGridView_DividendPersonVM";
            this.dataGridView_DividendPersonVM.Size = new System.Drawing.Size(368, 434);
            this.dataGridView_DividendPersonVM.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.dataGridView_changeHistory);
            this.groupBox2.Location = new System.Drawing.Point(19, 531);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(787, 358);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lịch sử thay đổi thông tin";
            // 
            // dataGridView_changeHistory
            // 
            this.dataGridView_changeHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_changeHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_changeHistory.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_changeHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_changeHistory.Location = new System.Drawing.Point(9, 25);
            this.dataGridView_changeHistory.Name = "dataGridView_changeHistory";
            this.dataGridView_changeHistory.Size = new System.Drawing.Size(772, 327);
            this.dataGridView_changeHistory.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_fullname_rever);
            this.groupBox1.Controls.Add(this.button_cancel);
            this.groupBox1.Controls.Add(this.button_confirm);
            this.groupBox1.Controls.Add(this.textBox_issue);
            this.groupBox1.Controls.Add(this.textBox_address);
            this.groupBox1.Controls.Add(this.textBox_cmnd);
            this.groupBox1.Controls.Add(this.textBox_fullname);
            this.groupBox1.Controls.Add(this.textBox_reason);
            this.groupBox1.Controls.Add(this.button_changeInfor);
            this.groupBox1.Controls.Add(this.label_cmnd);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBox_pay_money);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.button_pay);
            this.groupBox1.Controls.Add(this.label_cmnd_issue);
            this.groupBox1.Controls.Add(this.label_money);
            this.groupBox1.Controls.Add(this.label_tax);
            this.groupBox1.Controls.Add(this.label_address);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(19, 62);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(787, 463);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin người nhận";
            // 
            // textBox_fullname_rever
            // 
            this.textBox_fullname_rever.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_fullname_rever.Location = new System.Drawing.Point(228, 44);
            this.textBox_fullname_rever.Name = "textBox_fullname_rever";
            this.textBox_fullname_rever.Size = new System.Drawing.Size(423, 26);
            this.textBox_fullname_rever.TabIndex = 29;
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(131, 397);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(90, 39);
            this.button_cancel.TabIndex = 28;
            this.button_cancel.Text = "Hủy bỏ";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // button_confirm
            // 
            this.button_confirm.Location = new System.Drawing.Point(35, 397);
            this.button_confirm.Name = "button_confirm";
            this.button_confirm.Size = new System.Drawing.Size(90, 39);
            this.button_confirm.TabIndex = 27;
            this.button_confirm.Text = "Xác nhận";
            this.button_confirm.UseVisualStyleBackColor = true;
            this.button_confirm.Click += new System.EventHandler(this.button_confirm_Click);
            // 
            // textBox_issue
            // 
            this.textBox_issue.Location = new System.Drawing.Point(493, 83);
            this.textBox_issue.Name = "textBox_issue";
            this.textBox_issue.Size = new System.Drawing.Size(158, 26);
            this.textBox_issue.TabIndex = 26;
            // 
            // textBox_address
            // 
            this.textBox_address.Location = new System.Drawing.Point(228, 128);
            this.textBox_address.Name = "textBox_address";
            this.textBox_address.Size = new System.Drawing.Size(423, 26);
            this.textBox_address.TabIndex = 25;
            // 
            // textBox_cmnd
            // 
            this.textBox_cmnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_cmnd.Location = new System.Drawing.Point(228, 83);
            this.textBox_cmnd.Name = "textBox_cmnd";
            this.textBox_cmnd.Size = new System.Drawing.Size(158, 26);
            this.textBox_cmnd.TabIndex = 24;
            // 
            // textBox_fullname
            // 
            this.textBox_fullname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_fullname.Location = new System.Drawing.Point(228, 44);
            this.textBox_fullname.Name = "textBox_fullname";
            this.textBox_fullname.Size = new System.Drawing.Size(423, 26);
            this.textBox_fullname.TabIndex = 23;
            // 
            // textBox_reason
            // 
            this.textBox_reason.Location = new System.Drawing.Point(228, 174);
            this.textBox_reason.Multiline = true;
            this.textBox_reason.Name = "textBox_reason";
            this.textBox_reason.Size = new System.Drawing.Size(423, 71);
            this.textBox_reason.TabIndex = 22;
            // 
            // button_changeInfor
            // 
            this.button_changeInfor.Location = new System.Drawing.Point(32, 397);
            this.button_changeInfor.Name = "button_changeInfor";
            this.button_changeInfor.Size = new System.Drawing.Size(178, 39);
            this.button_changeInfor.TabIndex = 21;
            this.button_changeInfor.Text = "Thay đổi thông tin";
            this.button_changeInfor.UseVisualStyleBackColor = true;
            this.button_changeInfor.Click += new System.EventHandler(this.button_changeInfor_Click);
            // 
            // label_cmnd
            // 
            this.label_cmnd.AutoSize = true;
            this.label_cmnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_cmnd.Location = new System.Drawing.Point(224, 89);
            this.label_cmnd.Name = "label_cmnd";
            this.label_cmnd.Size = new System.Drawing.Size(0, 20);
            this.label_cmnd.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 20);
            this.label8.TabIndex = 18;
            this.label8.Text = "Lý do chi:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(404, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = "Ngày cấp: ";
            // 
            // textBox_pay_money
            // 
            this.textBox_pay_money.Enabled = false;
            this.textBox_pay_money.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_pay_money.Location = new System.Drawing.Point(228, 347);
            this.textBox_pay_money.Name = "textBox_pay_money";
            this.textBox_pay_money.Size = new System.Drawing.Size(423, 26);
            this.textBox_pay_money.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 350);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "Số tiền thanh toán:";
            // 
            // button_pay
            // 
            this.button_pay.Location = new System.Drawing.Point(424, 397);
            this.button_pay.Name = "button_pay";
            this.button_pay.Size = new System.Drawing.Size(227, 39);
            this.button_pay.TabIndex = 14;
            this.button_pay.Text = "Xác nhận thanh toán";
            this.button_pay.UseVisualStyleBackColor = true;
            this.button_pay.Click += new System.EventHandler(this.button_pay_Click);
            // 
            // label_cmnd_issue
            // 
            this.label_cmnd_issue.AutoSize = true;
            this.label_cmnd_issue.Location = new System.Drawing.Point(562, 89);
            this.label_cmnd_issue.Name = "label_cmnd_issue";
            this.label_cmnd_issue.Size = new System.Drawing.Size(89, 20);
            this.label_cmnd_issue.TabIndex = 13;
            this.label_cmnd_issue.Text = "08/12/2018";
            // 
            // label_money
            // 
            this.label_money.AutoSize = true;
            this.label_money.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_money.Location = new System.Drawing.Point(224, 307);
            this.label_money.Name = "label_money";
            this.label_money.Size = new System.Drawing.Size(0, 20);
            this.label_money.TabIndex = 12;
            // 
            // label_tax
            // 
            this.label_tax.AutoSize = true;
            this.label_tax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_tax.Location = new System.Drawing.Point(224, 263);
            this.label_tax.Name = "label_tax";
            this.label_tax.Size = new System.Drawing.Size(0, 20);
            this.label_tax.TabIndex = 11;
            // 
            // label_address
            // 
            this.label_address.AutoSize = true;
            this.label_address.Location = new System.Drawing.Point(224, 128);
            this.label_address.Name = "label_address";
            this.label_address.Size = new System.Drawing.Size(0, 20);
            this.label_address.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Họ tên người nhận tiền";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Địa chỉ:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 307);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Số tiền nhận được:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Số CMND:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 263);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Trừ thuế TNCN 5%: ";
            // 
            // textBox_cmnd_search
            // 
            this.textBox_cmnd_search.Location = new System.Drawing.Point(109, 20);
            this.textBox_cmnd_search.Name = "textBox_cmnd_search";
            this.textBox_cmnd_search.Size = new System.Drawing.Size(131, 26);
            this.textBox_cmnd_search.TabIndex = 7;
            this.textBox_cmnd_search.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_cmnd_KeyDown);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox11);
            this.tabPage2.Controls.Add(this.groupBox10);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1202, 910);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Thanh toán chuyển khoản";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox11.Controls.Add(this.dataGridView_cknh);
            this.groupBox11.Controls.Add(this.button_pay_cknh);
            this.groupBox11.Location = new System.Drawing.Point(873, 21);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(323, 1009);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Danh sách tài khoản thanh toán";
            // 
            // dataGridView_cknh
            // 
            this.dataGridView_cknh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_cknh.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_cknh.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_cknh.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_cknh.Location = new System.Drawing.Point(6, 80);
            this.dataGridView_cknh.Name = "dataGridView_cknh";
            this.dataGridView_cknh.Size = new System.Drawing.Size(317, 803);
            this.dataGridView_cknh.TabIndex = 0;
            // 
            // button_pay_cknh
            // 
            this.button_pay_cknh.AutoSize = true;
            this.button_pay_cknh.Location = new System.Drawing.Point(6, 37);
            this.button_pay_cknh.Name = "button_pay_cknh";
            this.button_pay_cknh.Size = new System.Drawing.Size(163, 37);
            this.button_pay_cknh.TabIndex = 1;
            this.button_pay_cknh.Text = "Thanh Toán CKNH";
            this.button_pay_cknh.UseVisualStyleBackColor = true;
            this.button_pay_cknh.Click += new System.EventHandler(this.button_pay_cknh_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox10.Controls.Add(this.button_Import_Bank);
            this.groupBox10.Controls.Add(this.dataGridView_bankAcounts);
            this.groupBox10.Location = new System.Drawing.Point(7, 258);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(860, 772);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Danh sách tài khoản";
            // 
            // button_Import_Bank
            // 
            this.button_Import_Bank.Location = new System.Drawing.Point(6, 35);
            this.button_Import_Bank.Name = "button_Import_Bank";
            this.button_Import_Bank.Size = new System.Drawing.Size(163, 37);
            this.button_Import_Bank.TabIndex = 10;
            this.button_Import_Bank.Text = "Nhập danh sách";
            this.button_Import_Bank.UseVisualStyleBackColor = true;
            this.button_Import_Bank.Click += new System.EventHandler(this.button_Import_Bank_Click_1);
            // 
            // dataGridView_bankAcounts
            // 
            this.dataGridView_bankAcounts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_bankAcounts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_bankAcounts.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_bankAcounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_bankAcounts.Location = new System.Drawing.Point(6, 78);
            this.dataGridView_bankAcounts.Name = "dataGridView_bankAcounts";
            this.dataGridView_bankAcounts.Size = new System.Drawing.Size(848, 568);
            this.dataGridView_bankAcounts.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.button_Update_Bank);
            this.groupBox9.Controls.Add(this.textBox_Bank_Name);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.textBox_Bank_Number);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.textBox_Bank_Benifier);
            this.groupBox9.Controls.Add(this.label16);
            this.groupBox9.Controls.Add(this.textBox_Bank_CMND);
            this.groupBox9.Controls.Add(this.label15);
            this.groupBox9.Location = new System.Drawing.Point(7, 21);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(860, 231);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Thêm tài khoản ngân hàng";
            // 
            // button_Update_Bank
            // 
            this.button_Update_Bank.Location = new System.Drawing.Point(157, 142);
            this.button_Update_Bank.Name = "button_Update_Bank";
            this.button_Update_Bank.Size = new System.Drawing.Size(163, 37);
            this.button_Update_Bank.TabIndex = 8;
            this.button_Update_Bank.Text = "Cập nhập thông tin";
            this.button_Update_Bank.UseVisualStyleBackColor = true;
            this.button_Update_Bank.Click += new System.EventHandler(this.button_Update_Bank_Click);
            // 
            // textBox_Bank_Name
            // 
            this.textBox_Bank_Name.Location = new System.Drawing.Point(560, 96);
            this.textBox_Bank_Name.Name = "textBox_Bank_Name";
            this.textBox_Bank_Name.Size = new System.Drawing.Size(200, 26);
            this.textBox_Bank_Name.TabIndex = 7;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(444, 102);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 20);
            this.label18.TabIndex = 6;
            this.label18.Text = "Ngân hàng";
            // 
            // textBox_Bank_Number
            // 
            this.textBox_Bank_Number.Location = new System.Drawing.Point(560, 48);
            this.textBox_Bank_Number.Name = "textBox_Bank_Number";
            this.textBox_Bank_Number.Size = new System.Drawing.Size(200, 26);
            this.textBox_Bank_Number.TabIndex = 5;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(444, 54);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 20);
            this.label17.TabIndex = 4;
            this.label17.Text = "Số tài khoản";
            // 
            // textBox_Bank_Benifier
            // 
            this.textBox_Bank_Benifier.Location = new System.Drawing.Point(157, 96);
            this.textBox_Bank_Benifier.Name = "textBox_Bank_Benifier";
            this.textBox_Bank_Benifier.Size = new System.Drawing.Size(200, 26);
            this.textBox_Bank_Benifier.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 100);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(126, 20);
            this.label16.TabIndex = 2;
            this.label16.Text = "Người hưởng thụ";
            // 
            // textBox_Bank_CMND
            // 
            this.textBox_Bank_CMND.Location = new System.Drawing.Point(157, 48);
            this.textBox_Bank_CMND.Name = "textBox_Bank_CMND";
            this.textBox_Bank_CMND.Size = new System.Drawing.Size(200, 26);
            this.textBox_Bank_CMND.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "Số CMND";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1202, 910);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách gốc";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.dataGridView_list_origin);
            this.panel1.Controls.Add(this.btn_ImportFile);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1196, 904);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.label_pageindex);
            this.panel2.Controls.Add(this.linkLabel1);
            this.panel2.Controls.Add(this.linkLabel4);
            this.panel2.Controls.Add(this.linkLabel2);
            this.panel2.Controls.Add(this.linkLabel3);
            this.panel2.Location = new System.Drawing.Point(527, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(107, 36);
            this.panel2.TabIndex = 11;
            // 
            // label_pageindex
            // 
            this.label_pageindex.AutoSize = true;
            this.label_pageindex.Location = new System.Drawing.Point(63, 10);
            this.label_pageindex.Name = "label_pageindex";
            this.label_pageindex.Size = new System.Drawing.Size(18, 20);
            this.label_pageindex.TabIndex = 10;
            this.label_pageindex.Text = "1";
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(1, 8);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(27, 20);
            this.linkLabel1.TabIndex = 6;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "<<";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel4
            // 
            this.linkLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.Location = new System.Drawing.Point(92, 8);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(18, 20);
            this.linkLabel4.TabIndex = 9;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = ">";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(34, 8);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(18, 20);
            this.linkLabel2.TabIndex = 7;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "<";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel3
            // 
            this.linkLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(116, 8);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(27, 20);
            this.linkLabel3.TabIndex = 8;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = ">>";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // dataGridView_list_origin
            // 
            this.dataGridView_list_origin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_list_origin.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_list_origin.Location = new System.Drawing.Point(6, 61);
            this.dataGridView_list_origin.Name = "dataGridView_list_origin";
            this.dataGridView_list_origin.Size = new System.Drawing.Size(1187, 840);
            this.dataGridView_list_origin.TabIndex = 0;
            // 
            // btn_ImportFile
            // 
            this.btn_ImportFile.Location = new System.Drawing.Point(6, 3);
            this.btn_ImportFile.Name = "btn_ImportFile";
            this.btn_ImportFile.Size = new System.Drawing.Size(172, 41);
            this.btn_ImportFile.TabIndex = 1;
            this.btn_ImportFile.Text = "Nhập danh sách";
            this.btn_ImportFile.UseVisualStyleBackColor = true;
            this.btn_ImportFile.Click += new System.EventHandler(this.btn_ImportFile_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox7);
            this.tabPage4.Controls.Add(this.groupBox6);
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1202, 910);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Danh sách đã chi";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.dataGridView_tran_bank);
            this.groupBox7.Location = new System.Drawing.Point(6, 591);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1190, 313);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Chi bằng CKNH";
            // 
            // dataGridView_tran_bank
            // 
            this.dataGridView_tran_bank.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_tran_bank.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_tran_bank.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_tran_bank.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_tran_bank.Location = new System.Drawing.Point(6, 23);
            this.dataGridView_tran_bank.Name = "dataGridView_tran_bank";
            this.dataGridView_tran_bank.Size = new System.Drawing.Size(1178, 284);
            this.dataGridView_tran_bank.TabIndex = 1;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.dataGridView_transaction_cash);
            this.groupBox6.Location = new System.Drawing.Point(6, 235);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1190, 350);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Chi bằng tiền mặt";
            // 
            // dataGridView_transaction_cash
            // 
            this.dataGridView_transaction_cash.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_transaction_cash.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_transaction_cash.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_transaction_cash.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_transaction_cash.Location = new System.Drawing.Point(6, 25);
            this.dataGridView_transaction_cash.Name = "dataGridView_transaction_cash";
            this.dataGridView_transaction_cash.Size = new System.Drawing.Size(1178, 319);
            this.dataGridView_transaction_cash.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.label_paid_StockBank);
            this.groupBox5.Controls.Add(this.label_paid_StockCash);
            this.groupBox5.Controls.Add(this.label_paid_Bank);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label_paid_Cash);
            this.groupBox5.Controls.Add(this.label_paid_StockTotal);
            this.groupBox5.Controls.Add(this.label_paid_MoneyTotal);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1190, 223);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Tổng hợp";
            // 
            // label_paid_StockBank
            // 
            this.label_paid_StockBank.AutoSize = true;
            this.label_paid_StockBank.Location = new System.Drawing.Point(415, 192);
            this.label_paid_StockBank.Name = "label_paid_StockBank";
            this.label_paid_StockBank.Size = new System.Drawing.Size(60, 20);
            this.label_paid_StockBank.TabIndex = 11;
            this.label_paid_StockBank.Text = "label21";
            // 
            // label_paid_StockCash
            // 
            this.label_paid_StockCash.AutoSize = true;
            this.label_paid_StockCash.Location = new System.Drawing.Point(415, 144);
            this.label_paid_StockCash.Name = "label_paid_StockCash";
            this.label_paid_StockCash.Size = new System.Drawing.Size(60, 20);
            this.label_paid_StockCash.TabIndex = 10;
            this.label_paid_StockCash.Text = "label20";
            // 
            // label_paid_Bank
            // 
            this.label_paid_Bank.AutoSize = true;
            this.label_paid_Bank.Location = new System.Drawing.Point(245, 192);
            this.label_paid_Bank.Name = "label_paid_Bank";
            this.label_paid_Bank.Size = new System.Drawing.Size(60, 20);
            this.label_paid_Bank.TabIndex = 9;
            this.label_paid_Bank.Text = "label18";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(31, 192);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(164, 20);
            this.label19.TabIndex = 8;
            this.label19.Text = "CHUYỂN KHOẢN NH";
            // 
            // label_paid_Cash
            // 
            this.label_paid_Cash.AutoSize = true;
            this.label_paid_Cash.Location = new System.Drawing.Point(245, 144);
            this.label_paid_Cash.Name = "label_paid_Cash";
            this.label_paid_Cash.Size = new System.Drawing.Size(60, 20);
            this.label_paid_Cash.TabIndex = 7;
            this.label_paid_Cash.Text = "label17";
            // 
            // label_paid_StockTotal
            // 
            this.label_paid_StockTotal.AutoSize = true;
            this.label_paid_StockTotal.Location = new System.Drawing.Point(415, 98);
            this.label_paid_StockTotal.Name = "label_paid_StockTotal";
            this.label_paid_StockTotal.Size = new System.Drawing.Size(60, 20);
            this.label_paid_StockTotal.TabIndex = 6;
            this.label_paid_StockTotal.Text = "label16";
            // 
            // label_paid_MoneyTotal
            // 
            this.label_paid_MoneyTotal.AutoSize = true;
            this.label_paid_MoneyTotal.Location = new System.Drawing.Point(245, 98);
            this.label_paid_MoneyTotal.Name = "label_paid_MoneyTotal";
            this.label_paid_MoneyTotal.Size = new System.Drawing.Size(60, 20);
            this.label_paid_MoneyTotal.TabIndex = 5;
            this.label_paid_MoneyTotal.Text = "label15";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(415, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 20);
            this.label14.TabIndex = 4;
            this.label14.Text = "SỐ CĐ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(241, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 20);
            this.label13.TabIndex = 3;
            this.label13.Text = " SỐ TIỀN ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(31, 144);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(164, 20);
            this.label12.TabIndex = 2;
            this.label12.Text = "CHI BẰNG TIỀN MẶT";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(31, 98);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(153, 20);
            this.label11.TabIndex = 1;
            this.label11.Text = "TỔNG TIỀN ĐÃ CHI";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(31, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "ĐÃ CHI";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox8);
            this.tabPage5.Controls.Add(this.dataGridView_not_pay_yet);
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1202, 910);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Danh sách chưa lưu ký";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.label_Stock_NOT_Paid);
            this.groupBox8.Controls.Add(this.label_Stock_Paid);
            this.groupBox8.Controls.Add(this.label_NOT_Paid_Money_Total);
            this.groupBox8.Controls.Add(this.label_Paid_Money_Total);
            this.groupBox8.Controls.Add(this.label29);
            this.groupBox8.Controls.Add(this.label30);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Location = new System.Drawing.Point(9, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1190, 179);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Tổng hợp";
            // 
            // label_Stock_NOT_Paid
            // 
            this.label_Stock_NOT_Paid.AutoSize = true;
            this.label_Stock_NOT_Paid.Location = new System.Drawing.Point(415, 144);
            this.label_Stock_NOT_Paid.Name = "label_Stock_NOT_Paid";
            this.label_Stock_NOT_Paid.Size = new System.Drawing.Size(60, 20);
            this.label_Stock_NOT_Paid.TabIndex = 8;
            this.label_Stock_NOT_Paid.Text = "label28";
            // 
            // label_Stock_Paid
            // 
            this.label_Stock_Paid.AutoSize = true;
            this.label_Stock_Paid.Location = new System.Drawing.Point(241, 144);
            this.label_Stock_Paid.Name = "label_Stock_Paid";
            this.label_Stock_Paid.Size = new System.Drawing.Size(60, 20);
            this.label_Stock_Paid.TabIndex = 7;
            this.label_Stock_Paid.Text = "label28";
            // 
            // label_NOT_Paid_Money_Total
            // 
            this.label_NOT_Paid_Money_Total.AutoSize = true;
            this.label_NOT_Paid_Money_Total.Location = new System.Drawing.Point(415, 98);
            this.label_NOT_Paid_Money_Total.Name = "label_NOT_Paid_Money_Total";
            this.label_NOT_Paid_Money_Total.Size = new System.Drawing.Size(60, 20);
            this.label_NOT_Paid_Money_Total.TabIndex = 6;
            this.label_NOT_Paid_Money_Total.Text = "label27";
            // 
            // label_Paid_Money_Total
            // 
            this.label_Paid_Money_Total.AutoSize = true;
            this.label_Paid_Money_Total.Location = new System.Drawing.Point(241, 98);
            this.label_Paid_Money_Total.Name = "label_Paid_Money_Total";
            this.label_Paid_Money_Total.Size = new System.Drawing.Size(60, 20);
            this.label_Paid_Money_Total.TabIndex = 5;
            this.label_Paid_Money_Total.Text = "label28";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(415, 47);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(72, 20);
            this.label29.TabIndex = 4;
            this.label29.Text = "CÒN LẠI";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(241, 47);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(68, 20);
            this.label30.TabIndex = 3;
            this.label30.Text = "ĐÃ TRẢ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(31, 144);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(111, 20);
            this.label31.TabIndex = 2;
            this.label31.Text = "SỐ CỔ ĐÔNG";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(31, 98);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(94, 20);
            this.label32.TabIndex = 1;
            this.label32.Text = "TỔNG TIỀN";
            // 
            // dataGridView_not_pay_yet
            // 
            this.dataGridView_not_pay_yet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_not_pay_yet.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_not_pay_yet.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_not_pay_yet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_not_pay_yet.Location = new System.Drawing.Point(3, 188);
            this.dataGridView_not_pay_yet.Name = "dataGridView_not_pay_yet";
            this.dataGridView_not_pay_yet.Size = new System.Drawing.Size(1196, 719);
            this.dataGridView_not_pay_yet.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1234, 967);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Tra cứu cổ tức";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_transaciton_history_person)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_DividendPersonVM)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_changeHistory)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_cknh)).EndInit();
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_bankAcounts)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_list_origin)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_tran_bank)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_transaction_cash)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_not_pay_yet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btn_ImportFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridView dataGridView_list_origin;
        private System.Windows.Forms.Button button_pay_cknh;
        private System.Windows.Forms.DataGridView dataGridView_cknh;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_cmnd_search;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label_money;
        private System.Windows.Forms.Label label_tax;
        private System.Windows.Forms.Label label_address;
        private System.Windows.Forms.Label label_cmnd_issue;
        private System.Windows.Forms.DataGridView dataGridView_DividendPersonVM;
        private System.Windows.Forms.Button button_pay;
        private System.Windows.Forms.TextBox textBox_pay_money;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridView_transaciton_history_person;
        private System.Windows.Forms.Label label_cmnd;
        private System.Windows.Forms.Button button_changeInfor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_reason;
        private System.Windows.Forms.TextBox textBox_cmnd;
        private System.Windows.Forms.TextBox textBox_fullname;
        private System.Windows.Forms.TextBox textBox_address;
        private System.Windows.Forms.TextBox textBox_issue;
        private System.Windows.Forms.Button button_confirm;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label_paid_StockBank;
        private System.Windows.Forms.Label label_paid_StockCash;
        private System.Windows.Forms.Label label_paid_Bank;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label_paid_Cash;
        private System.Windows.Forms.Label label_paid_StockTotal;
        private System.Windows.Forms.Label label_paid_MoneyTotal;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dataGridView_tran_bank;
        private System.Windows.Forms.DataGridView dataGridView_transaction_cash;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dataGridView_not_pay_yet;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label_NOT_Paid_Money_Total;
        private System.Windows.Forms.Label label_Paid_Money_Total;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label_Stock_NOT_Paid;
        private System.Windows.Forms.Label label_Stock_Paid;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.TextBox textBox_fullname_rever;
        private System.Windows.Forms.DataGridView dataGridView_changeHistory;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox textBox_Bank_Name;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox_Bank_Number;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox_Bank_Benifier;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox_Bank_CMND;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button_Update_Bank;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.DataGridView dataGridView_bankAcounts;
        private System.Windows.Forms.Button button_Import_Bank;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label_pageindex;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel3;
    }
}

