//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TraCuuCoTuc.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class bank_account
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bank_account()
        {
            this.transactions = new HashSet<transaction>();
        }
    
        public long id { get; set; }
        public string cmnd_number { get; set; }
        public string beneficiaries { get; set; }
        public string number_account { get; set; }
        public string bank { get; set; }
        public Nullable<System.DateTime> created { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<transaction> transactions { get; set; }
    }
}
