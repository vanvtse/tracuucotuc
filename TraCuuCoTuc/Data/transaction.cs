//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TraCuuCoTuc.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class transaction
    {
        public long id { get; set; }
        public string cnmd { get; set; }
        public string type { get; set; }
        public string full_name { get; set; }
        public Nullable<decimal> receive_money { get; set; }
        public Nullable<decimal> tax { get; set; }
        public Nullable<decimal> real_recevie_money { get; set; }
        public Nullable<System.DateTime> created { get; set; }
        public long original_id { get; set; }
        public Nullable<long> bank_account_id { get; set; }
        public bool is_paid { get; set; }
        public Nullable<System.DateTime> paid_date { get; set; }
    
        public virtual bank_account bank_account { get; set; }
        public virtual original_listing original_listing { get; set; }
    }
}
