﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TraCuuCoTuc.Helper;
using TraCuuCoTuc.Model;

namespace TraCuuCoTuc
{
    public partial class Form1
    {
        private void pay_cknh(object sender, EventArgs e)
        {

            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx";
            saveDialog.FilterIndex = 2;

            if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // workbook.SaveAs(saveDialog.FileName);
                var rs = this.dataContext.transactions.Where(x => x.bank_account != null).Where(x => x.is_paid == false).Include(c => c.bank_account);
                foreach (var item in rs)
                {
                    item.is_paid = true;
                    item.paid_date = DateTime.Now;
                }

                var export = rs.Select(c => new transactionVM
                {
                    bank = c.bank_account.bank,
                    beneficiaries = c.bank_account.beneficiaries,
                    cmnd_number = c.cnmd,
                    full_name = c.full_name,
                    get_revc_money = c.receive_money,
                    get_revc_real_money = c.real_recevie_money,
                    number_account = c.bank_account.number_account,
                    tax = c.tax
                }).ToList();


                dataContext.SaveChanges();
                ExportExcel.CreateExcelDocument(export, saveDialog.FileName);
                RefreshUI();
                MessageBox.Show("Export Successful");
            }
        }
    }
}
